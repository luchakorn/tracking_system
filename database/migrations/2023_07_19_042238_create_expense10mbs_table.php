<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpense10mbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense10mbs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('province_id');
            $table->string('disaster_type',50)->nullable();
            $table->string('amphoe',50)->nullable();
            $table->date('disaster_date')->nullable();
            $table->integer('cost_budget')->nullable();
            $table->integer('material_budget')->nullable();
            $table->integer('maintanance_budget')->nullable();
            $table->integer('powerfuel_budget')->nullable();
            $table->integer('powerelec_budget')->nullable();
            $table->integer('trip_budget')->nullable();
            $table->integer('food_budget')->nullable();
            $table->integer('repay_budget')->nullable();
            $table->integer('balance_budget')->nullable();
            $table->string('no_book',50)->nullable();
            $table->date('no_date')->nullable();
            $table->longText('remark')->nullable();
            $table->string('user_id',15)->nullable();
            $table->string('created_by')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('restored_by')->nullable();
            $table->timestamp('restored_at')->nullable();
            $table->enum('status', ['A','R','P','X']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense10mbs');
    }
}
