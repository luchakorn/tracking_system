<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuSub1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_sub1s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('menu_main_id');
            $table->string('link');
            $table->string('status');
            $table->enum('sub_menu_access', ['Administrator','Admin', 'User', 'General']);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_sub1s');
    }
}
