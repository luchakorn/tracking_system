<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class menuSub1 extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'icon',
        'link',
        'status',
        'sub_menu_access',
    ];

}
