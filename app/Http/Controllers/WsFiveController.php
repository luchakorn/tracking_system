<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class WsFiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ws5Dashboard()
    {
        return view('ws5-graph-dashboard');
    }
    public function ws5ManageData()
    {
        return view('ws5-manage-data');
    }
    public function ws5ManageData20()
    {
        return view('ws5-manage-data20');
    }
    public function ws5Approve()
    {
        return view('ws5-approve');
    }
    public function ws5Recovery()
    {
        $getAllData10mb= DB::table('expense10mbs')
            ->select('*')
            ->where('status', '=', 'A')
            ->get();
        
        $getAllData20mb= DB::table('expense20mbs')
            ->select('*')
            ->where('status', '=', 'A')
            ->get();
        return view('ws5-recovery',compact('getAllData10mb','getAllData20mb'));
    }
    public function inputData10mb(Request $request)
    {
        // dd($request);
        $data = array();
        $data["province_id"] =  Auth::user()->province_id;
        $data["disaster_type"] = $request->disasterType;
        $data["amphoe"] = $request->amphoe;
        $data["disaster_date"] = $request->disasterDate;
        $data["cost_budget"] = $request->costBudget;
        $data["material_budget"] = $request->meterialBudget;
        $data["maintanance_budget"] = $request->maintananceBudget;
        $data["powerfuel_budget"] = $request->powerfuelBudget;
        $data["powerelec_budget"] = $request->powerelecBudget;
        $data["trip_budget"] = $request->tripBudet;
        $data["food_budget"] = $request->foodBudget;
        $data["repay_budget"] = $request->repayBudget;
        $data["balance_budget"] = $request->balanceBudget;
        $data["no_book"] = $request->noBook;
        $data["no_date"] = $request->noDate;
        $data["remark"] = $request->description;
        $data["user_id"] =  Auth::user()->account_id;
        $data["created_at"] = Carbon::now()->toDateTimeString();
        $data["created_by"] = Auth::user()->name;
        $data["status"] = "A";
    
        $insertExpnse10mb = DB::table('expense10mbs')->insert($data);
        toast('Your Form as been submited!','success');
        return view('ws5-manage-data');
    } 
    public function inputData20mb(Request $request)
    {
        // dd($request);
        $data = array();
        $data["province_id"] =  Auth::user()->province_id;
        $data["disaster_type"] = $request->disasterType;
        $data["amphoe"] = $request->amphoe;
        $data["disaster_date"] = $request->disasterDate;
        $data["subsistence"] = $request->subsistence;
        $data["social_welfare"] = $request->socialWelfare;
        $data["medical_health"] = $request->medicalHealth;
        $data["agriculture"] = $request->agriculture;
        $data["disaster"] = $request->disaster;
        $data["helpvictim"] = $request->helpVictim;
        $data["repay_budget"] = $request->repayBudget;
        $data["balance_budget"] = $request->balanceBudget;
        $data["no_book"] = $request->noBook;
        $data["no_date"] = $request->noDate;
        $data["remark"] = $request->description;
        $data["user_id"] =  Auth::user()->account_id;
        $data["created_at"] = Carbon::now()->toDateTimeString();
        $data["created_by"] = Auth::user()->name;
        $data["status"] = "A";
    
        $insertExpnse20mb = DB::table('expense20mbs')->insert($data);
        toast('Your Form as been submited!','success');
        return view('ws5-manage-data20');
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
