<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;


class WsOneController extends Controller
{
    // public function ws1Dashboard()
    // {
    //     $getSumCose = DB::table('expense10mbs')
    //         ->select(DB::raw('SUM(cost_budget) as cose_budget'))
    //         // ->where('province_id', '=', $id)
    //         ->where('status', '=', 'A')
    //         ->get();
            
    //     $getSumMaterial= DB::table('expense10mbs')
    //         ->select(DB::raw('SUM(material_budget) as material_budget'))
    //         // ->where('province_id', '=', $id)
    //         ->where('status', '=', 'A')
    //         ->get();

    //      $getSumMaintanance= DB::table('expense10mbs')
    //         ->select(DB::raw('SUM(maintanance_budget) as maintanance_budget'))
    //         // ->where('province_id', '=', $id)
    //         ->where('status', '=', 'A')
    //         ->get();

    //     $getSumPowerfuel= DB::table('expense10mbs')
    //         ->select(DB::raw('SUM(powerfuel_budget) as powerfuel_budget'))
    //         // ->where('province_id', '=', $id)
    //         ->where('status', '=', 'A')
    //         ->get();

    //     $getSumPowerelec= DB::table('expense10mbs')
    //         ->select(DB::raw('SUM(powerelec_budget) as powerelec_budget'))
    //         // ->where('province_id', '=', $id)
    //         ->where('status', '=', 'A')
    //         ->get();
        
    //     $getSumTrip= DB::table('expense10mbs')
    //         ->select(DB::raw('SUM(trip_budget) as trip_budget'))
    //         // ->where('province_id', '=', $id)
    //         ->where('status', '=', 'A')
    //         ->get();
    
    //     return view('ws1-graph-dashboard',compact('getSumCose','getSumMaterial','getSumMaintanance','getSumPowerfuel','getSumPowerelec','getSumTrip'));
    // }
    public function ws1ManageData()
    {
        return view('ws1-manage-data');
    }
    public function ws1ManageData20()
    {
        return view('ws1-manage-data20');
    }
    public function ws1Approve()
    {
        return view('ws1-approve');
    }
    public function ws1Recovery()
    {
        $getAllData10mb= DB::table('expense10mbs')
            ->select('*')
            ->where('status', '=', 'A')
            ->get();
        
        $getAllData20mb= DB::table('expense20mbs')
            ->select('*')
            ->where('status', '=', 'A')
            ->get();
        return view('ws1-recovery',compact('getAllData10mb','getAllData20mb'));
    }
    public function inputData10mb(Request $request)
    {
        // dd($request);
        $data = array();
        $data["province_id"] =  Auth::user()->province_id;
        $data["disaster_type"] = $request->disasterType;
        $data["amphoe"] = $request->amphoe;
        $data["disaster_date"] = $request->disasterDate;
        $data["cost_budget"] = $request->costBudget;
        $data["material_budget"] = $request->meterialBudget;
        $data["maintanance_budget"] = $request->maintananceBudget;
        $data["powerfuel_budget"] = $request->powerfuelBudget;
        $data["powerelec_budget"] = $request->powerelecBudget;
        $data["trip_budget"] = $request->tripBudet;
        $data["food_budget"] = $request->foodBudget;
        $data["repay_budget"] = $request->repayBudget;
        $data["balance_budget"] = $request->balanceBudget;
        $data["no_book"] = $request->noBook;
        $data["no_date"] = $request->noDate;
        $data["remark"] = $request->description;
        $data["user_id"] =  Auth::user()->account_id;
        $data["created_at"] = Carbon::now()->toDateTimeString();
        $data["created_by"] = Auth::user()->name;
        $data["status"] = "A";
    
        $insertExpnse10mb = DB::table('expense10mbs')->insert($data);
        toast('Your Form as been submited!','success');
        return view('ws1-manage-data');
    } 
    public function inputData20mb(Request $request)
    {
        // dd($request);
        $data = array();
        $data["province_id"] =  Auth::user()->province_id;
        $data["disaster_type"] = $request->disasterType;
        $data["amphoe"] = $request->amphoe;
        $data["disaster_date"] = $request->disasterDate;
        $data["subsistence"] = $request->subsistence;
        $data["social_welfare"] = $request->socialWelfare;
        $data["medical_health"] = $request->medicalHealth;
        $data["agriculture"] = $request->agriculture;
        $data["disaster"] = $request->disaster;
        $data["helpvictim"] = $request->helpVictim;
        $data["repay_budget"] = $request->repayBudget;
        $data["balance_budget"] = $request->balanceBudget;
        $data["no_book"] = $request->noBook;
        $data["no_date"] = $request->noDate;
        $data["remark"] = $request->description;
        $data["user_id"] =  Auth::user()->account_id;
        $data["created_at"] = Carbon::now()->toDateTimeString();
        $data["created_by"] = Auth::user()->name;
        $data["status"] = "A";
    
        $insertExpnse20mb = DB::table('expense20mbs')->insert($data);
        toast('Your Form as been submited!','success');
        return view('ws1-manage-data20');
    } 

}

