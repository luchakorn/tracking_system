<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class dashboardController extends Controller
{
    public function index(Request $REQUEST)
{
    
    $getSumCose = DB::table('expense10mbs')
    ->select(DB::raw('SUM(cost_budget) as cose_budget'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();
    
$getSumMaterial= DB::table('expense10mbs')
    ->select(DB::raw('SUM(material_budget) as material_budget'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

 $getSumMaintanance= DB::table('expense10mbs')
    ->select(DB::raw('SUM(maintanance_budget) as maintanance_budget'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

$getSumPowerfuel= DB::table('expense10mbs')
    ->select(DB::raw('SUM(powerfuel_budget) as powerfuel_budget'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

$getSumPowerelec= DB::table('expense10mbs')
    ->select(DB::raw('SUM(powerelec_budget) as powerelec_budget'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

$getSumTrip= DB::table('expense10mbs')
    ->select(DB::raw('SUM(trip_budget) as trip_budget'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

$getSumSubsistence = DB::table('expense20mbs')
    ->select(DB::raw('SUM(subsistence) as subsistence'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();
    
$getSumSocial= DB::table('expense20mbs')
    ->select(DB::raw('SUM(social_welfare) as social_welfare'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

 $getSumMedical= DB::table('expense20mbs')
    ->select(DB::raw('SUM(medical_health) as medical_health'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

$getSumAgriculture= DB::table('expense20mbs')
    ->select(DB::raw('SUM(agriculture) as agriculture'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

$getSumDisaster= DB::table('expense20mbs')
    ->select(DB::raw('SUM(disaster) as disaster'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

$getSumHelpvictim= DB::table('expense20mbs')
    ->select(DB::raw('SUM(helpvictim) as helpvictim'))
    ->where('province_id', '=', $REQUEST->pID)
    ->where('status', '=', 'A')
    ->get();

return view('dashboard',compact('getSumCose','getSumMaterial','getSumMaintanance','getSumPowerfuel','getSumPowerelec','getSumTrip','getSumSubsistence','getSumSocial','getSumMedical','getSumAgriculture','getSumDisaster','getSumHelpvictim'));
}


    }
  