<?php

namespace App\Actions\Fortify;

use App\Models\Team;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ])->validate();

        if (count($input) >= 7) {

            $animal_new_id = 'IMG_' . Carbon::now()->format('ymd') . Carbon::now()->format('his');

            $name_gen = $animal_new_id;
            $img_ext = strtolower($input['user_img']->getClientOriginalExtension());
            $img_name = $name_gen . '.' . $img_ext;

            $upload_location = 'user_image/imgUpload/';
            $full_path = $upload_location . $img_name;
            $input['user_img']->move($upload_location, $img_name);
          
        } else {
            $full_path = "";
        }


        return User::create([
            'account_id' => 'A' . Carbon::now()->format('ymd') . Carbon::now()->format('his'),
            'account_token' => Hash::make('A' . Carbon::now()->format('ymd') . Carbon::now()->format('his')),
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            // 'app_access' => $input['zoo_access'],
            // 'app_access_hash' => Hash::make($input['zoo_access']),
            'profile_photo_path' => $full_path,
            'authen_access' => $input['authen_access'],
            'province_id' => $input['provinceId'],
            'status' => 'A',
        ]);

    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function createTeam(User $user)
    {
        $user->ownedTeams()->save(Team::forceCreate([
            'user_id' => $user->id,
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            'personal_team' => true,
        ]));
    }
}
