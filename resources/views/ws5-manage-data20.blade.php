<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Tracking System | Management </title>

	<!-- Bootstrap -->
	<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- bootstrap-wysiwyg -->
	<link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
	<!-- Select2 -->
	<link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
	<!-- Switchery -->
	<link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
	<!-- starrr -->
	<link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
	<!-- bootstrap-daterangepicker -->
	<link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- left menu -->
            @include("layouts.menu")
            <!-- end left menu -->

            <!-- menu-top-bar-->
            @include("layouts.menu-top-bar")
            <!-- menu-top-bar-->
            <!-- page content -->
            <div class="right_col" role="main">
                <!-- **************************Content************************** -->
				<div class="">
					<!-- <div class="page-title">
						<div class="title_left">
							<h3>Input Form</h3>
						</div>

						<div class="title_right">
							<div class="col-md-5 col-sm-5  form-group pull-right top_search">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search for...">
									<span class="input-group-btn">
										<button class="btn btn-default" type="button">Go!</button>
									</span>
								</div>
							</div>
						</div>
					</div> -->
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 ">
							<div class="x_panel">
								<div class="x_title">
									<p style="font-size: 16px;">ฟอร์มรายละเอียดรายการเบิกจ่ายเงินทดรองราชการเพื่อช่วยเหลือผู้ประสบภัยพิบัติกรณีฉุกเฉิน <small>(เชิงบรรเทาความเดือดร้อน 20 ล้านบาทต่อทุกภัย)</small></p>
									<ul class="nav navbar-right panel_toolbox">
										<!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
										</li> -->
										<!-- <li class="dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
												aria-expanded="false"><i class="fa fa-wrench"></i></a>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a class="dropdown-item" href="#">Settings 1</a>
												<a class="dropdown-item" href="#">Settings 2</a>
											</div>
										</li> -->
										<!-- <li><a class="close-link"><i class="fa fa-close"></i></a>
										</li> -->
									</ul>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<br />
									<form class="form-horizontal form-label-left" method="post" action="{{ route('create-inputData20mb') }}">
									@csrf 
									<div class="form-group row">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">ประเภทภัย</label>
											<div class="col-md-4 col-sm-4 ">
												<select name= "disasterType" class="select2_single form-control" tabindex="-1">
													<option></option>
													<option value="อุทกภัย">อุทกภัย</option>
													<option value="ภัยแล้ง">ภัยแล้ง</option>
													<option value="ภัยหนาว">ภัยหนาว</option>
													<option value="อัคคีภัย">อัคคีภัย</option>
													<option value="วาตภัย">วาตภัย</option>
													<option value="ฝนทิ้งช่วง">ฝนทิ้งช่วง</option>
													<option value="ภัยจากลูกเห็บ">ภัยจากลูกเห็บ</option>
													<option value="ฝนแล้ง">ฝนแล้ง</option>
													<option value="โรค/ศัตรูพืชระบาด">โรค/ศัตรูพืชระบาด</option>
													<option value="โรคระบาดสัตว์">โรคระบาดสัตว์</option>
													<option value="อื่น ๆ">อื่น ๆ</option>
													
												</select>
											</div>
											<label style="text-align:right;" class="control-label col-md-2 col-sm-2 ">วันที่เกิดภัย <span
													class="required">*</span>
											</label>
											<div class="col-md-3 col-sm-3 ">
												<input name= "disasterDate" id="birthday" class="date-picker form-control"
													placeholder="dd-mm-yyyy" type="text" required="required" type="text"
													onfocus="this.type='date'" onmouseover="this.type='date'"
													onclick="this.type='date'" onblur="this.type='text'"
													onmouseout="timeFunctionLong(this)">
												<script>
													function timeFunctionLong(input) {
														setTimeout(function () {
															input.type = 'text';
														}, 60000);
													}
												</script>
											</div>
										</div>
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">อำเภอที่เกิดภัย</label>
											<div class="col-md-9 col-sm-9 ">
												<select name= "amphoe" class="select2_single form-control" tabindex="-1">
													<option></option>
													<option value="เมือง">เมือง</option>
													<option value="เวียงชัย">เวียงชัย</option>
													<option value="แม่ลาว">แม่ลาว</option>
													<option value="แม่จัน">แม่จัน</option>
													<option value="ดอยหลวง">ดอยหลวง</option>
													<option value="พาน">พาน</option>
													<option value="พญาเม็งราย">พญาเม็งราย</option>
													<option value="เวียงเชียงรุ้ง">เวียงเชียงรุ้ง</option>
													<option value="ป่าแดด">ป่าแดด</option>
													<option value="แม่สรวย">แม่สรวย</option>
													<option value="เชียงแสน">เชียงแสน</option>
													<option value="ขุนตาล">ขุนตาล</option>
													<option value="แม่สาย">แม่สาย</option>
													<option value="เทิง">เทิง</option>
													<option value="แม่ฟ้าหลวง">แม่ฟ้าหลวง</option>
													<option value="เวียงป่าเป้า">เวียงป่าเป้า</option>
													<option value="เวียงแก่น">เวียงแก่น</option>
													<option value="เชียงของ">เชียงของ</option>
												</select>
											</div>
										</div>
										<!-- <div class="form-group row">
											<label class="control-label col-md-3 col-sm-3 ">วันที่เกิดภัย <span
													class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-69 ">
												<input id="birthday" class="date-picker form-control"
													placeholder="dd-mm-yyyy" type="text" required="required" type="text"
													onfocus="this.type='date'" onmouseover="this.type='date'"
													onclick="this.type='date'" onblur="this.type='text'"
													onmouseout="timeFunctionLong(this)">
												<script>
													function timeFunctionLong(input) {
														setTimeout(function () {
															input.type = 'text';
														}, 60000);
													}
												</script>
											</div>
										</div> -->
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">ด้าน 5.1 การดำรงชีพ</label>
											<div class="col-md-9 col-sm-9 ">
												<input name= "subsistence" type="number" min="0" value="0" class="form-control">
											</div>
										</div>
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">ด้าน 5.2 สังคมสงเคราะห์</label>
											<div class="col-md-9 col-sm-9 ">
												<input name= "socialWelfare" type="number" min="0" value="0" class="form-control">
											</div>
										</div>
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">ด้าน 5.3 การแพทย์และการสาธารณสุข</label>
											<div class="col-md-9 col-sm-9 ">
												<input name= "medicalHealth" type="number" min="0" value="0" class="form-control">
											</div>
										</div>
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">ด้าน 5.4 การเกษตร</label>
											<div class="col-md-9 col-sm-9 ">
												<input name= "agriculture" type="number" min="0" value="0" class="form-control">
											</div>
										</div>
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">ด้าน 5.5 บรรเทาสาธารณภัย</label>
											<div class="col-md-9 col-sm-9 ">
												<input name= "disaster" type="number" min="0" value="0" class="form-control">
											</div>
										</div>
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">ด้าน 5.6 การปฏับัติงานให้ความช่วยเหลือผู้ประสบภัย</label>
											<div class="col-md-9 col-sm-9 ">
												<input name= "helpVictim" type="number" min="0" value="0" class="form-control">
											</div>
										</div>
										<div class="form-group row ">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">จำนวนเงินที่ได้รับชดใช้คืน (บาท)</label>
											<div class="col-md-4 col-sm-4 ">
												<input name= "repayBudget" type="number" min="0" value="0" class="form-control">
											</div>
											<label style="text-align:right;" class="control-label col-md-2 col-sm-2 ">คงเหลือจำนวนทั้งสิ้น (บาท)</label>
											<div class="col-md-3 col-sm-3 ">
												<input name= "balanceBudget" type="number" min="0" value="0" class="form-control">
											</div>
										</div>
										<!-- <div class="form-group row ">
											<label class="control-label col-md-3 col-sm-3 ">คงเหลือจำนวนทั้งสิ้น (บาท)</label>
											<div class="col-md-9 col-sm-9 ">
												<input type="number" class="form-control" placeholder="Budget">
											</div>
										</div> -->
										<div class="form-group row">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">เลขที่หนังสือจังหวัด </label>
											<div class="col-md-4 col-sm-4 ">
												<input name= "noBook" type="text" class="form-control" placeholder="Text" 
													placeholder="Disabled Input">
											</div>
											<label style="text-align:right;" class="control-label col-md-2 col-sm-2 ">วันที่หนังสือจังหวัด <span
													class="required">*</span>
											</label>
											<div class="col-md-3 col-sm-3 ">
												<input name= "noDate" id="birthday" class="date-picker form-control"
													placeholder="dd-mm-yyyy" type="text" required="required" type="text"
													onfocus="this.type='date'" onmouseover="this.type='date'"
													onclick="this.type='date'" onblur="this.type='text'"
													onmouseout="timeFunctionLong(this)">
												<script>
													function timeFunctionLong(input) {
														setTimeout(function () {
															input.type = 'text';
														}, 60000);
													}
												</script>
											</div>
										</div>
										<div class="form-group row">
											<label style="text-align:right;" class="control-label col-md-3 col-sm-3 ">หมายเหตุ </label>
											<div class="col-md-9 col-sm-9 ">
											<textarea name= "description" class="form-control" rows="5" id="comment"></textarea>
										</div>
										</div>
										<div class="ln_solid"></div>
										<div class="form-group">
											<div class="col-md-9 col-sm-9  offset-md-3">
												<button type="button" class="btn btn-primary">Cancel</button>
												<button type="reset" class="btn btn-primary">Reset</button>
												<button type="submit" class="btn btn-success">Submit</button>
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
                <!-- **************************End Content************************** -->

            </div>
            <!-- /page content -->

            <!-- footer content -->
			@include("layouts.footer")
            <!-- /footer content -->
			@include('sweetalert::alert')
        </div>
    </div>

  <!-- jQuery -->
	<script src="../vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<!-- FastClick -->
	<script src="../vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="../vendors/nprogress/nprogress.js"></script>
	<!-- bootstrap-progressbar -->
	<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- iCheck -->
	<script src="../vendors/iCheck/icheck.min.js"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="../vendors/moment/min/moment.min.js"></script>
	<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap-wysiwyg -->
	<script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
	<script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
	<script src="../vendors/google-code-prettify/src/prettify.js"></script>
	<!-- jQuery Tags Input -->
	<script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
	<!-- Switchery -->
	<script src="../vendors/switchery/dist/switchery.min.js"></script>
	<!-- Select2 -->
	<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
	<!-- Parsley -->
	<script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
	<!-- Autosize -->
	<script src="../vendors/autosize/dist/autosize.min.js"></script>
	<!-- jQuery autocomplete -->
	<script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
	<!-- starrr -->
	<script src="../vendors/starrr/dist/starrr.js"></script>
	<!-- Custom Theme Scripts -->
	<script src="../build/js/custom.min.js"></script>

</body>
</html>
