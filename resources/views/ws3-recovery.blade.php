<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Tracking System | Data table </title>

    <!-- Bootstrap -->
    <link href="cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Datatables -->
    
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- left menu -->
            @include("layouts.menu")
            <!-- end left menu -->

            <!-- menu-top-bar-->
            @include("layouts.menu-top-bar")
            <!-- menu-top-bar-->
            <!-- page content -->
            <div class="right_col" role="main">
                <!-- **************************Content************************** -->
                <div class="row">
                <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                  <p style="font-size: 16px;">รายละเอียดรายการใช้จ่ายเงินทดรองราชการในเชิงป้องกันหรือยับยั้งภัยพิบัติฉุกเฉิน  <small>(วงเงิน 10 ล้านบาท ต่อทุกภัย)</small></p>
                  </div>
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>ประเภทภัย</th>
                          <th>อำเภอ</th>
                          <th width="50">วันที่เกิดภัย</th>
                          <th>ค่าแรงงาน/ค่าจ้างเหมา</th>
                          <th>ค่าวัสดุ อุปกรณ์</th>
                          <th>ค่าซ่อมแซม เครื่องมือ เครื่องจักรกลฯ</th>
                          <th>ค่าจัดหาพลังงาน เชื้อเพลิงฯ</th>
                          <th>ค่าจัดหาพลังงานไฟฟ้า</th>
                          <!-- <th>ค่าตอบแทน/ค่าใช้จ่ายในการเดินทางฯ</th>
                          <th>ค่าอาหารจัดเลี้ยงเจ้าหน้าที่ฯ</th> -->
                          <!-- <th>จำนวนเงินที่ได้รับชดใช้คืน (บาท)</th>
                          <th>คงเหลือจำนวนทั้งสิ้น (บาท)</th>
                          <th>เลขที่หนังสือจังหวัด</th>
                          <th>วันที่หนังสือจังหวัด</th>
                          <th>หมายเหตุ</th>
                        </tr> -->
                      </thead>

                      <tbody>
                      @foreach($getAllData10mb as $rows)
                      <tr>
                          <td>{{$rows->disaster_type}}</td>
                          <td>{{$rows->amphoe}}</td>
                          <td>{{$rows->disaster_date}}</td>
                          <td>{{$rows->cost_budget}}</td>
                          <td>{{$rows->material_budget}}</td>
                          <td>{{$rows->powerfuel_budget}}</td>
                          <td>{{$rows->powerelec_budget}}</td>
                          <td>{{$rows->trip_budget}}</td>
                          <!-- <td>{{$rows->repay_budget}}</td>
                          <td>{{$rows->no_book}}</td>
                          <td>{{$rows->no_date}}</td>
                          <td>{{$rows->remark}}</td> -->
                        </tr>
                        @endforeach
                   
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>
              </div>

              <div style= "margin: 10px;"> &emsp; 
                </div>
              <div class="x_title">
                  <p style="font-size: 16px;">รายละเอียดรายการเบิกจ่ายเงินทดรองราชการเพื่อช่วยเหลือผู้ประสบภัยพิบัติกรณีฉุกเฉิน <small> (เชิงบรรเทาความเดือดร้อน 20 ล้านบาทต่อทุกภัย)</small></p>
                  </div>
                  <div class="x_content">
                      <div class="row">
                          <div class="col-sm-12">
                            <div class="card-box table-responsive">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action" style="width:100%">
                      <thead>
                        <tr>
                          <th>ประเภทภัย</th>
                          <th>อำเภอ</th>
                          <th width="100">วันที่เกิดภัย</th>
                          <th>ด้าน 5.1 </th>
                          <th>ด้าน 5.2 </th>
                          <th>ด้าน 5.3 </th>
                          <th>ด้าน 5.4 </th>
                          <th>ด้าน 5.5 </th>
                          <th>ด้าน 5.6 </th>
                      </thead>

                      <tbody>
                      @foreach($getAllData20mb as $rows)
                      <tr>
                          <td>{{$rows->disaster_type}}</td>
                          <td>{{$rows->amphoe}}</td>
                          <td>{{$rows->disaster_date}}</td>
                          <td>{{$rows->subsistence}}</td>
                          <td>{{$rows->social_welfare}}</td>
                          <td>{{$rows->medical_health}}</td>
                          <td>{{$rows->agriculture}}</td>
                          <td>{{$rows->disaster}}</td>
                          <td>{{$rows->helpvictim}}</td>
                          <!-- <td>{{$rows->repay_budget}}</td>
                          <td>{{$rows->no_book}}</td>
                          <td>{{$rows->no_date}}</td>
                          <td>{{$rows->remark}}</td> -->
                        </tr>
                        @endforeach
                   
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- **************************End Content************************** -->

            </div>
            <!-- /page content -->

            <!-- footer content -->
            <footer>
                <div class="pull-right">
                    Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
   <script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script>


</body>

</html>