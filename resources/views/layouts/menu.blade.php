<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><img width="30" src="{{ asset('production/images/chorpor.ico') }}">
                <span>Tracking System</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <!-- <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{ asset('production/images/luchakorn.jpg') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>Luchakorn BUU</h2>
            </div>
        </div> -->
        <!-- /menu profile quick info -->

        <br />
        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <!-- <h3>Work Sheet 1-9 GeoSpatial Database</h3> -->
                <ul class="nav side-menu">
                    @foreach($menuMainModel as $rows)

                    @if ($rows->id == "1" && Auth::user()->province_id==0 )
                    <li><a href="./{{$rows->link}}"><i class="{{$rows->icon}}"></i>{{$rows->name}}</a></li>
                    @else
                    @if (Auth::user()->province_id+1==$rows->id)
                    <li><a data-toggle="tooltip" data-placement="right" title="{{$rows->description}}"><i
                                class="{{$rows->icon}}"></i>{{$rows->name}} <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            @foreach ($rows->submenus1 as $submenu )
                            @if (Auth::user()->authen_access == $submenu->sub_menu_access)

                            @elseif(Auth::user()->province_id!=0 && Auth::user()->authen_access=='Admin' && ($submenu->sub_menu_access=='Admin' ||  $submenu->sub_menu_access=='User') )
                            <?php 
                           $pid = ($submenu->menu_main_id)-1;
                            ?> 
                            <li><a href="./{{$submenu->link}}?pID=<?=$pid?>">{{$submenu->name}}</a>1</li>
                            @else
                            <?php 
                           $pid = ($submenu->menu_main_id)-1;
                            ?> 
                            <li><a href="./{{$submenu->link}}?pID=<?=$pid?>">{{$submenu->name}}</a>2</li>
                            @endif
                            @endforeach
                        </ul>
                    </li>
                    @elseif(Auth::user()->province_id==0)
                    <li><a data-toggle="tooltip" data-placement="right" title="{{$rows->description}}"><i
                                class="{{$rows->icon}}"></i>{{$rows->name}} <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            @foreach ($rows->submenus1 as $submenu )
                            @if (Auth::user()->authen_access == $submenu->sub_menu_access)
                           
                            @elseif(Auth::user()->province_id==0 && Auth::user()->authen_access=='Super Admin' && $submenu->sub_menu_access=='Admin' )
                            
                            <?php  $pid = ($submenu->menu_main_id)-1; ?> 
                            <li><a href="./{{$submenu->link}}?pID=<?=$pid?>">{{$submenu->name}}</a></li>
                     
                            @endif
                            @endforeach
                        </ul>
                    </li>
                    @endif
                    @endif

                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
            </form>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>