        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a> 
              </div>
              <nav class="nav navbar-nav">
              <ul class=" navbar-right"> 
                <div style="position:absolute">  
                @if(Auth::user()->province_id==1)
                <p style= "margin-top: 5px;"> จังหวัดเชียงราย </p>
                @elseif(Auth::user()->province_id==2)
                <p style= "margin-top: 5px;"> จังหวัดสุพรรณบุรี </p>
                @elseif(Auth::user()->province_id==3)
                <p style= "margin-top: 5px;"> จังหวัดกาฬสินธุ์ </p>
                @elseif(Auth::user()->province_id==4)
                <p style= "margin-top: 5px;"> จังหวัดจันทบุรี </p>
                @elseif(Auth::user()->province_id==5)
                <p style= "margin-top: 5px;"> จังหวัดนครศรีธรรมราช </p>
                @else
                <p style= "margin-top: 5px;"> ภาพรวม 5 จังหวัด </p>
                @endif
                </div>
                <li class="nav-item dropdown open" style="padding-left: 15px;">
                  <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                   @if(Auth::user()->profile_photo_path != "" || Auth::user()->profile_photo_path != null)
                  <img src="{{Auth::user()->profile_photo_path}}" alt="">
                    @else
                    <img src="{{ asset('user_image/main.png') }}" alt="">
                    @endif
                    {{Auth::user()->name}}
                  </a>
                  <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item"  href="javascript:;"> Profile</a>
                      <a class="dropdown-item"  href="javascript:;">
                        <!-- <span class="badge bg-red pull-right">50%</span> -->
                        <span>Settings</span>
                      </a>
                  <a class="dropdown-item"  href="javascript:;">Help</a>
                  <form method="POST" action="{{ route('logout') }}">
                @csrf
                    <a data-toggle="tooltip" data-placement="top" class="dropdown-item"  href="" onclick="event.preventDefault();
                                                this.closest('form').submit();"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </form>
                <!-- <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html" onclick="event.preventDefault();
                                                this.closest('form').submit();">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a> -->
                  </div>
                </li>

                <li role="presentation" class="nav-item dropdown open">
                  <a href="javascript:;" class="dropdown-toggle info-number" id="navbarDropdown1" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul class="dropdown-menu list-unstyled msg_list" role="menu" aria-labelledby="navbarDropdown1">
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                        <span>
                          <span>จังหวัดเชียงราย</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                        <span>
                          <span>จังหวัดสุพรรณบุรี</span>
                          <span class="time">5 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                        <span>
                          <span>จังหวัดกาฬสินธุ์</span>
                          <span class="time">10 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                        <span>
                          <span>จังหวัดจันทบุรี</span>
                          <span class="time">20 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="dropdown-item">
                        <!-- <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span> -->
                        <span>
                          <span>จังหวัดนครศรีธรรมราช</span>
                          <span class="time">20 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>

                    <li class="nav-item">
                      <div class="text-center">
                        <a class="dropdown-item">
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->