<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Appthapma-Register</title>
	<link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- iCheck -->
	<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	<!-- bootstrap-wysiwyg -->
	<link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
	<!-- Select2 -->
	<link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
	<!-- Switchery -->
	<link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
	<!-- starrr -->
	<link href="../vendors/starrr/dist/starrr.css" rel="stylesheet">
	<!-- bootstrap-daterangepicker -->
	<link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="../build/css/custom.min.css" rel="stylesheet">

    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <style>
/*Profile Pic Start*/
.picture-container{
    position: relative;
    cursor: pointer;
    text-align: center;
}
.picture{
    width: 106px;
    height: 106px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
    border-radius: 50%;
    margin: 0px auto;
    overflow: hidden;
 
}
.picture:hover{
    border-color: #A9A9A9;
}
.content.ct-wizard-green .picture:hover{
    border-color: #27C0A2;
}
.content.ct-wizard-blue .picture:hover{
    border-color: #27C0A2;
}
.content.ct-wizard-orange .picture:hover{
    border-color: #27C0A2;
}
.content.ct-wizard-red .picture:hover{
    border-color: #27C0A2;
}
.picture input[type="file"] {
    cursor: pointer;
    display: block;
    height: 100%;
    left: 0;
    opacity: 0 !important;
    position: absolute;
    top: 0;
    width: 100%;
}

.picture-src{
    width: 100%;
    
}
/*Profile Pic End*/
    </style>
  </head>
  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
          @csrf
              <h1>Create Account</h1>
              <div>
              <div class="" align="center">
              <div class="container">
    <div class="picture-container">
        <div class="picture">
            <img src="{{asset('user_image/main.png')}}" class="picture-src" id="wizardPicturePreview" title="">
            <input type="file" name="user_img" id="wizard-picture" class="">
        </div>
         <h6 class="">Choose Picture</h6>

    </div>
</div>
</div>
              </div>
              <div>
              <input type="text" class="form-control" placeholder="Username" required=""   id="name"  name="name" :value="old('name')" required autofocus autocomplete="off"/>
              </div>
              <div>
                <input type="email" class="form-control" id="email" name="email" :value="old('email')" required placeholder="Email" autocomplete="off"/>
              </div>
<div>
<p>
Super Admin:
											<input type="radio" class="flat" name="authen_access" id="authen_accessS" value="Super Admin"
												checked="" required /> &emsp;
                                                Admin:
											<input type="radio" class="flat" name="authen_access" id="authen_accessA" value="Admin" /> &emsp;
                                            User
											<input type="radio" class="flat" name="authen_access" id="authen_accessU" value="User" /> &emsp;
										</p>
</div>
<div>
                      <select class="select2_single form-control" name='provinceId'>
													<option>Select Province</option>
                          <option value="0">5 จังหวัด </option>
													<option value="1">จังหวัดเชียงราย </option>
													<option value="2">จังหวัดสุพรรณบุรี</option>
													<option value="3">จังหวัดกาฬสินธุ์ </option>
													<option value="4">จังหวัดจันทบุรี </option>
													<option value="5">จังหวัดนครศรีธรรมราช</option>
                      </select>
</div>
<br>
              <div>

                <input type="password" class="form-control" placeholder="Password" id="password"  name="password" required autocomplete="off" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password-Confirm" id="password_confirmation" name="password_confirmation" required autocomplete="off" />
              </div>
              <div>
                <!-- <a class="btn btn-default submit" href="index.html">Submit</a> -->
                <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered ?') }}
                </a>

                <x-jet-button class="ml-4" style="background-color:#27C0A2;">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                @include("layouts.footer-login")
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>


    	<!-- jQuery -->
	<script src="../vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="../vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
	<!-- FastClick -->
	<script src="../vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="../vendors/nprogress/nprogress.js"></script>
	<!-- bootstrap-progressbar -->
	<script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	<!-- iCheck -->
	<script src="../vendors/iCheck/icheck.min.js"></script>
	<!-- bootstrap-daterangepicker -->
	<script src="../vendors/moment/min/moment.min.js"></script>
	<script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap-wysiwyg -->
	<script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
	<script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
	<script src="../vendors/google-code-prettify/src/prettify.js"></script>
	<!-- jQuery Tags Input -->
	<script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
	<!-- Switchery -->
	<script src="../vendors/switchery/dist/switchery.min.js"></script>
	<!-- Select2 -->
	<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
	<!-- Parsley -->
	<script src="../vendors/parsleyjs/dist/parsley.min.js"></script>
	<!-- Autosize -->
	<script src="../vendors/autosize/dist/autosize.min.js"></script>
	<!-- jQuery autocomplete -->
	<script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
	<!-- starrr -->
	<script src="../vendors/starrr/dist/starrr.js"></script>
	<!-- Custom Theme Scripts -->
	<script src="../build/js/custom.min.js"></script>
  <script>
$(document).ready(function(){
// Prepare the preview for profile picture
    $("#wizard-picture").change(function(){
        readURL(this);
    });
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
  </body>
</html>
