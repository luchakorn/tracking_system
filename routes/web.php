<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WsOneController;
use App\Http\Controllers\WsTwoController;
use App\Http\Controllers\WsThreeController;
use App\Http\Controllers\WsFourController;
use App\Http\Controllers\WsFiveController;
use App\Http\Controllers\WsSixController;
use App\Http\Controllers\WsSevenController;
use App\Http\Controllers\WsEightController;
use App\Http\Controllers\WsNightController;
use App\Http\Controllers\dashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {

Route::GET('/dashboard', [dashboardController::class, 'index'])->name('dashboard');
// Route::GET('/dashboard/getvalue/{id}', [dashboardController::class, 'getvalue'])->name('getvalue');
Route::GET('/ws1-graph-dashboard/{id}', [WsOneController::class, 'ws1Dashboard'])->name('ws1-graph-dashboard');
Route::GET('/ws1-manage-data', [WsOneController::class, 'ws1ManageData'])->name('ws1-manage-data');
Route::GET('/ws1-manage-data20', [WsOneController::class, 'ws1ManageData20'])->name('ws1-manage-data20');
Route::GET('/ws1-recovery', [WsOneController::class, 'ws1Recovery'])->name('ws1-recovery');
Route::GET('/ws1-approve', [WsOneController::class, 'ws1Approve'])->name('ws1-approve');
Route::POST('/create-inputData10mb', [WsOneController::class, 'inputData10mb'])->name('create-inputData10mb');
Route::POST('/create-inputData20mb', [WsOneController::class, 'inputData20mb'])->name('create-inputData20mb');


Route::GET('/ws2-graph-dashboard', [WsTwoController::class, 'ws2Dashboard'])->name('ws2-graph-dashboard');
Route::GET('/ws2-manage-data', [WsTwoController::class, 'ws2ManageData'])->name('ws2-manage-data');
Route::GET('/ws2-manage-data20', [WsTwoController::class, 'ws2ManageData20'])->name('ws2-manage-data20');
Route::GET('/ws2-recovery', [WsTwoController::class, 'ws2Recovery'])->name('ws2-recovery');
Route::GET('/ws2-approve', [WsTwoController::class, 'ws2Approve'])->name('ws2-approve');


Route::GET('/ws3-graph-dashboard', [WsThreeController::class, 'ws3Dashboard'])->name('ws3-graph-dashboard');
Route::GET('/ws3-manage-data', [WsThreeController::class, 'ws3ManageData'])->name('ws3-manage-data');
Route::GET('/ws3-manage-data20', [WsThreeController::class, 'ws3ManageData20'])->name('ws3-manage-data20');
Route::GET('/ws3-recovery', [WsThreeController::class, 'ws3Recovery'])->name('ws3-recovery');
Route::GET('/ws3-approve', [WsThreeController::class, 'ws3Approve'])->name('ws3-approve');


Route::GET('/ws4-graph-dashboard', [WsFourController::class, 'ws4Dashboard'])->name('ws4-graph-dashboard');
Route::GET('/ws4-manage-data', [WsFourController::class, 'ws4ManageData'])->name('ws4-manage-data');
Route::GET('/ws4-manage-data20', [WsFourController::class, 'ws4ManageData20'])->name('ws4-manage-data20');
Route::GET('/ws4-recovery', [WsFourController::class, 'ws4Recovery'])->name('ws4-recovery');
Route::GET('/ws4-approve', [WsFourController::class, 'ws4Approve'])->name('ws4-approve');


Route::GET('/ws5-graph-dashboard', [WsFiveController::class, 'ws5Dashboard'])->name('ws5-graph-dashboard');
Route::GET('/ws5-manage-data', [WsFiveController::class, 'ws5ManageData'])->name('ws5-manage-data');
Route::GET('/ws5-manage-data20', [WsFiveController::class, 'ws5ManageData20'])->name('ws5-manage-data20');
Route::GET('/ws5-recovery', [WsFiveController::class, 'ws5Recovery'])->name('ws5-recovery');
Route::GET('/ws5-approve', [WsFiveController::class, 'ws5Approve'])->name('ws5-approve');



Route::GET('/ws6-graph-dashboard', [WsSixController::class, 'ws6Dashboard'])->name('ws6-graph-dashboard');
Route::GET('/ws6-manage-data', [WsSixController::class, 'ws6ManageData'])->name('ws6-manage-data');
Route::GET('/ws6-recovery', [WsSixController::class, 'ws6Recovery'])->name('ws6-recovery');


Route::GET('/ws7-graph-dashboard', [WsSevenController::class, 'ws7Dashboard'])->name('ws7-graph-dashboard');
Route::GET('/ws7-manage-data', [WsSevenController::class, 'ws7ManageData'])->name('ws7-manage-data');
Route::GET('/ws7-recovery', [WsSevenController::class, 'ws7Recovery'])->name('ws7-recovery');


Route::GET('/ws8-graph-dashboard', [WsEightController::class, 'ws8Dashboard'])->name('ws8-graph-dashboard');
Route::GET('/ws8-manage-data', [WsEightController::class, 'ws8ManageData'])->name('ws8-manage-data');
Route::GET('/ws8-recovery', [WsEightController::class, 'ws8Recovery'])->name('ws8-recovery');


Route::GET('/ws9-graph-dashboard', [WsNightController::class, 'ws9Dashboard'])->name('ws9-graph-dashboard');
Route::GET('/ws9-manage-data', [WsNightController::class, 'ws9ManageData'])->name('ws9-manage-data');
Route::GET('/ws9-recovery', [WsNightController::class, 'ws9Recovery'])->name('ws9-recovery');

});
